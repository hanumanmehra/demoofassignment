trigger RestricatedInvoiceDeletion on Invoice__c (before delete) 
{
for(Invoice__c invoice:[Select Id From Invoice__c Where Id IN(Select Invoice__c From Line_Item__c)])
{
    Trigger.oldMap.get(invoice.Id).addError('Cannot delete Invoice witn in LineItem');
}
}